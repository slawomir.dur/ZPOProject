#include "producttable.h"
#include <QHeaderView>
#include <QHBoxLayout>

ProductTable::ProductTable(Database *db, QWidget *parent) : QWidget(parent), db(db)
{
    if (db == nullptr)
    {
        throw ProductTable::NoDataBaseConnectionError("Cannot connect to database. Database variable set to nullptr.");
    }
    else
    {
        db->connect();
    }

    headers.push_back("Name");
    headers.push_back("Category");
    headers.push_back("Size");
    headers.push_back("Make");
    headers.push_back("Price");

    TableModel = new QSqlTableModel(this);
    TableModel->setTable("PRODUCTS");

    for (int i = 0; i < TableModel->columnCount(); ++i)
    {
        TableModel->setHeaderData(i, Qt::Horizontal, headers[i]);
    }
    TableView = new QTableView(this);
    TableView->setModel(TableModel);
    TableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    TableView->setSelectionMode(QAbstractItemView::SingleSelection);
    TableView->resizeColumnsToContents();
    TableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    TableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    selection = TableView->selectionModel();

    TableModel->select();

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(TableView);
    setLayout(layout);
}

void ProductTable::repaint()
{
    if (db->connect())
    {
        TableModel->select();
        TableView->repaint();
    }

}
