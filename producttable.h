#ifndef PRODUCTTABLE_H
#define PRODUCTTABLE_H

#include <QWidget>
#include <QSqlTableModel>
#include <QTableView>
#include <QItemSelectionModel>
#include <vector>
#include <exception>

#include "qdatabasewrapper.h"

class ProductTable : public QWidget
{
public:
    class NoDataBaseConnectionError : public std::exception
    {
    private:
        std::string msg_;
    public:
        NoDataBaseConnectionError(const char* msg) : msg_(msg) {}
        NoDataBaseConnectionError(const std::string& msg) : msg_(msg) {}
        virtual ~NoDataBaseConnectionError() {}
        virtual const char* what() const throw() override { return msg_.c_str(); }
    };
    Q_OBJECT

private:
    QTableView* TableView = nullptr;
    QSqlTableModel* TableModel = nullptr;
    QItemSelectionModel* selection = nullptr;
    std::vector<QString> headers;
    Database *db = nullptr;
public:
    explicit ProductTable(Database *db, QWidget *parent = nullptr);
    QItemSelectionModel* selectionModel() { return selection; }
    QTableView* getTableView() { return TableView; }
signals:

public slots:
    void repaint();
};

#endif // PRODUCTTABLE_H
