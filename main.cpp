#include <QApplication>

#include "mainwindow.h"
#include "qdatabasewrapper.h"

int main(int argv, char** argc)
{
    QApplication app(argv, argc);

    const std::string database_name = "shop.db";

    Database db("HOST", Database::DatabaseDriver::SQLITE, database_name.c_str());
    db.setExceptions(Database::ExceptionHandling::NOTHROW);

    db.connect();

    QVariantList names {"NAZWA", "KATEGORIA", "ROZMIAR", "MARKA", "CENA"};
    QVariantList types {"TEXT", "TEXT", "TEXT", "TEXT", "TEXT"};

    DatabaseTable productsTable("PRODUCTS", names, types);
    db.createTable(productsTable);


    MainWindow window(&db);
    window.setMinimumSize(800,600);
    window.setWindowTitle("Program");
    window.show();


    return app.exec();
}
