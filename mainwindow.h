#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QDialog>

#include "producttable.h"
#include "qdatabasewrapper.h"

class AddWindow;
class FindWindow;
class ChangeWindow;
class SortWindow;
class RemoveWindow;

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Database *db;
    QPushButton* DelButton;
    QPushButton* AddButton;
    QPushButton* ChangeButton;
    ProductTable *Table;

    AddWindow *addWindow;
    ChangeWindow *changeWindow;
public:
    explicit MainWindow(Database *db, QWidget *parent = nullptr);
    Database* getDatabase() { return db; }
signals:
    void dataChanged();
public slots:
    void add_clicked();
    void change_clicked();
    void remove_clicked();
};

class AddWindow : public QDialog
{
    Q_OBJECT
public:
    explicit AddWindow(QWidget *parent = nullptr);
    QPushButton* getAddButton() { return add; }
private:
    void makeConnections();
private:
    QLineEdit *nameEdit;
    QLineEdit *categoryEdit;
    QLineEdit *sizeEdit;
    QLineEdit *makeEdit;
    QLineEdit *priceEdit;
    QPushButton *add;
    QPushButton *cancel;
signals:
    void dataChanged();
public slots:
    void add_slot();
    void cancel_slot();
};

class ChangeWindow : public QDialog
{
    Q_OBJECT
public:
    explicit ChangeWindow(QWidget *parent = nullptr);
    void setOldData(DataSet* data_ptr) { this->old_data = data_ptr; }
private:
    void makeConnections();
private:
    QLineEdit *old_name_edit;
    QLineEdit *old_category_edit;
    QLineEdit *old_size_edit;
    QLineEdit *old_make_edit;
    QLineEdit *old_price_edit;

    QLineEdit *new_name_edit;
    QLineEdit *new_category_edit;
    QLineEdit *new_size_edit;
    QLineEdit *new_make_edit;
    QLineEdit *new_price_edit;

    QPushButton *updateButton;
    QPushButton *cancelButton;

    DataSet *old_data = nullptr;
signals:
    void dataChanged();
public slots:
    void update_slot();
    void cancel_slot();
};

#endif // MAINWINDOW_H
