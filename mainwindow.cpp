#include "mainwindow.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>

MainWindow::MainWindow(Database *db, QWidget *parent) : QMainWindow(parent), db(db)
{
    addWindow = new AddWindow(this);
    changeWindow = new ChangeWindow(this);

    DelButton = new QPushButton("Delete", this);
    AddButton = new QPushButton("Add", this);
    ChangeButton = new QPushButton("Change", this);
    Table = new ProductTable(db, this);

    DelButton->setMinimumHeight(DelButton->height()*1.8);
    AddButton->setMinimumHeight(AddButton->height()*1.8);
    ChangeButton->setMinimumHeight(ChangeButton->height()*1.8);

    this->setStyleSheet("MainWindow{"
                        "background-color: #b9a6ff;"
                        "}"

                        "QPushButton{"
                        "background-color: #6b49e3;"
                        "border-radius: 4px;"
                        "color: white;"
                        "font-size: 20px;"
                        "font-weight: bold;"
                        "font-family: \"Comic Sans MS\";"
                        "}"

                        "QPushButton:hover{"
                        "font-size: 22px;"
                        "background-color: #7c5ee6;"
                        "}");

    QVBoxLayout* PrimeLayout = new QVBoxLayout;
    QHBoxLayout* LeftLayout = new QHBoxLayout;
    QHBoxLayout* RightLayout = new QHBoxLayout;
    QHBoxLayout* ButtonsLayout = new QHBoxLayout;

    ButtonsLayout->addWidget(AddButton);
    ButtonsLayout->addWidget(ChangeButton);
    ButtonsLayout->addWidget(DelButton);

    LeftLayout->addWidget(Table);
    RightLayout->addLayout(ButtonsLayout);

    PrimeLayout->addLayout(LeftLayout);
    PrimeLayout->addLayout(RightLayout);

    QWidget* centralWidget = new QWidget;
    centralWidget->setLayout(PrimeLayout);

    this->setCentralWidget(centralWidget);

    connect(AddButton, SIGNAL(clicked(bool)), this, SLOT(add_clicked()));
    connect(ChangeButton, SIGNAL(clicked(bool)), this, SLOT(change_clicked()));
    connect(DelButton, SIGNAL(clicked(bool)), this, SLOT(remove_clicked()));
    connect(addWindow, SIGNAL(dataChanged()), Table, SLOT(repaint()));
    connect(changeWindow, SIGNAL(dataChanged()), Table, SLOT(repaint()));
    connect(this, SIGNAL(dataChanged()), Table, SLOT(repaint()));
}

void MainWindow::add_clicked()
{
    addWindow->setModal(true);
    addWindow->exec();
}

void MainWindow::change_clicked()
{
    QItemSelectionModel* selection = Table->selectionModel();
    if (selection->hasSelection())
    {
           QModelIndexList index_list = selection->selectedRows();
           std::unordered_multimap<const DataSet::DataType, QVariant> data;
           for (auto& index : index_list)
           {
               QAbstractItemModel* table = Table->getTableView()->model();

               int row = index.row();
               for (int i = table->columnCount() - 1; i >= 0; --i)
               {
                   data.emplace(DataSet::DataType::TEXT, table->data(table->index(row, i)));
               }
           }

           DataSet data_to_update(data);

           changeWindow->setOldData(&data_to_update);
           changeWindow->setModal(true);
           changeWindow->exec();
    }
    else
    {
        QMessageBox msg(this);
        msg.setModal(true);
        msg.setStandardButtons(QMessageBox::Ok);
        msg.setText("No rows selected!");
        msg.exec();
    }
}

void MainWindow::remove_clicked()
{
    QItemSelectionModel* selection = Table->selectionModel();
    if (selection->hasSelection())
    {
       QMessageBox msg(this);
       msg.setText("Do you really want to delete this record?");
       msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
       int reply = msg.exec();
       if (reply == QMessageBox::Yes)
       {
           QModelIndexList index_list = selection->selectedRows();
           std::unordered_multimap<const DataSet::DataType, QVariant> data;
           for (auto& index : index_list)
           {
               QAbstractItemModel* table = Table->getTableView()->model();

               int row = index.row();
               for (int i = table->columnCount() - 1; i >= 0; --i)
               {
                   data.emplace(DataSet::DataType::TEXT, table->data(table->index(row, i)));
               }
           }
           DataSet data_to_delete(data);

           if (db->connect())
           {
               if (db->removeData(db->getTablesCreated().at(0), data_to_delete))
               {
                   emit dataChanged();
               }
               else
               {
                   QMessageBox msg(this);
                   msg.setModal(true);
                   msg.setStandardButtons(QMessageBox::Ok);
                   msg.setText("Could not remove data due to internal error!");
                   msg.exec();
               }
           }
           else
           {
               QMessageBox msg(this);
               msg.setModal(true);
               msg.setStandardButtons(QMessageBox::Ok);
               msg.setText("Could not connect to database!");
               msg.exec();
           }
       }
       else if (reply == QMessageBox::No)
       {
           msg.close();
       }


    }
    else
    {
        QMessageBox msg(this);
        msg.setModal(true);
        msg.setStandardButtons(QMessageBox::Ok);
        msg.setText("No rows selected!");
        msg.exec();
    }
}

AddWindow::AddWindow(QWidget *parent) : QDialog(parent)
{
    QHBoxLayout *nameBox = new QHBoxLayout;
    QHBoxLayout *categoryBox = new QHBoxLayout;
    QHBoxLayout *sizeBox = new QHBoxLayout;
    QHBoxLayout *makeBox = new QHBoxLayout;
    QHBoxLayout *priceBox = new QHBoxLayout;

    QHBoxLayout *buttons = new QHBoxLayout;

    QVBoxLayout *primeLayout = new QVBoxLayout;

    QLabel *title = new QLabel(tr("New product"));
    QLabel *nameLabel = new QLabel(tr("Name:"));
    QLabel *categoryLabel = new QLabel(tr("Category:"));
    QLabel *sizeLabel = new QLabel(tr("Size:"));
    QLabel *makeLabel = new QLabel(tr("Make:"));
    QLabel *priceLabel = new QLabel(tr("Price:"));

    nameEdit = new QLineEdit;
    categoryEdit = new QLineEdit;
    sizeEdit = new QLineEdit;
    makeEdit = new QLineEdit;
    priceEdit = new QLineEdit;

    add = new QPushButton(tr("Add"));
    cancel = new QPushButton(tr("Cancel"));

    nameBox->addWidget(nameLabel);
    nameBox->addWidget(nameEdit);

    categoryBox->addWidget(categoryLabel);
    categoryBox->addWidget(categoryEdit);

    sizeBox->addWidget(sizeLabel);
    sizeBox->addWidget(sizeEdit);

    makeBox->addWidget(makeLabel);
    makeBox->addWidget(makeEdit);

    priceBox->addWidget(priceLabel);
    priceBox->addWidget(priceEdit);

    buttons->addWidget(add);
    buttons->addWidget(cancel);

    primeLayout->addWidget(title);
    primeLayout->addLayout(nameBox);
    primeLayout->addLayout(categoryBox);
    primeLayout->addLayout(sizeBox);
    primeLayout->addLayout(makeBox);
    primeLayout->addLayout(priceBox);
    primeLayout->addLayout(buttons);

    setLayout(primeLayout);

    makeConnections();
}

void AddWindow::makeConnections()
{
    connect(add, SIGNAL(clicked(bool)), this, SLOT(add_slot()));
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(cancel_slot()));
}

void AddWindow::add_slot()
{
    auto parent_base = parentWidget();
    if (auto parent_proper = dynamic_cast<MainWindow*>(parent_base))
    {
        auto db = parent_proper->getDatabase();
        if (db->connect())
        {

            std::unordered_multimap<const DataSet::DataType, QVariant> data_elements;
            data_elements.emplace(DataSet::DataType::TEXT, priceEdit->text());
            data_elements.emplace(DataSet::DataType::TEXT, makeEdit->text());
            data_elements.emplace(DataSet::DataType::TEXT, sizeEdit->text());
            data_elements.emplace(DataSet::DataType::TEXT, categoryEdit->text());
            data_elements.emplace(DataSet::DataType::TEXT, nameEdit->text());

            DataSet data(data_elements);

            if (db->insertData(db->getTablesCreated().at(0), data))
            {
                nameEdit->setText("");
                categoryEdit->setText("");
                sizeEdit->setText("");
                makeEdit->setText("");
                priceEdit->setText("");

                emit dataChanged();
            }
            else
            {
                QMessageBox msg(this);
                msg.setModal(true);
                msg.setStandardButtons(QMessageBox::Ok);
                msg.setText("Could not insert data due to internal error!");
                msg.exec();
            }
        }
        else
        {
            QMessageBox msg(this);
            msg.setModal(true);
            msg.setStandardButtons(QMessageBox::Ok);
            msg.setText("Could not connect to database!");
            msg.exec();
        }

    }
    else
    {
        QMessageBox msg(this);
        msg.setModal(true);
        msg.setStandardButtons(QMessageBox::Ok);
        msg.setText("dynamic_cast<MainWindow*> failure -> contact with developer!");
        msg.exec();
    }

}

void AddWindow::cancel_slot()
{
    close();
}

ChangeWindow::ChangeWindow(QWidget *parent) : QDialog(parent)
{
    QVBoxLayout *superPrimeLayout = new QVBoxLayout;
    QHBoxLayout *primeLayout = new QHBoxLayout;

    QVBoxLayout *new_data_layout = new QVBoxLayout;

    QHBoxLayout *new_name_layout = new QHBoxLayout;
    QHBoxLayout *new_category_layout = new QHBoxLayout;
    QHBoxLayout *new_size_layout = new QHBoxLayout;
    QHBoxLayout *new_make_layout = new QHBoxLayout;
    QHBoxLayout *new_price_layout = new QHBoxLayout;

    QGroupBox *new_box = new QGroupBox(tr("New data"));

    QLabel *new_name_label = new QLabel(tr("Name: "));
    QLabel *new_category_label = new QLabel(tr("Category: "));
    QLabel *new_size_label = new QLabel(tr("Size: "));
    QLabel *new_make_label = new QLabel(tr("Make: "));
    QLabel *new_price_label = new QLabel(tr("Price: "));

    new_name_edit = new QLineEdit;
    new_category_edit = new QLineEdit;
    new_size_edit = new QLineEdit;
    new_make_edit = new QLineEdit;
    new_price_edit = new QLineEdit;

    new_name_layout->addWidget(new_name_label);
    new_name_layout->addWidget(new_name_edit);

    new_category_layout->addWidget(new_category_label);
    new_category_layout->addWidget(new_category_edit);

    new_size_layout->addWidget(new_size_label);
    new_size_layout->addWidget(new_size_edit);

    new_make_layout->addWidget(new_make_label);
    new_make_layout->addWidget(new_make_edit);

    new_price_layout->addWidget(new_price_label);
    new_price_layout->addWidget(new_price_edit);

    new_data_layout->addLayout(new_name_layout);
    new_data_layout->addLayout(new_category_layout);
    new_data_layout->addLayout(new_size_layout);
    new_data_layout->addLayout(new_make_layout);
    new_data_layout->addLayout(new_price_layout);

    new_box->setLayout(new_data_layout);

    primeLayout->addWidget(new_box);

    updateButton = new QPushButton(tr("Update"), this);
    cancelButton = new QPushButton(tr("Cancel"), this);

    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(updateButton);
    buttonsLayout->addWidget(cancelButton);

    superPrimeLayout->addLayout(primeLayout);
    superPrimeLayout->addLayout(buttonsLayout);

    this->setLayout(superPrimeLayout);

    makeConnections();
}

void ChangeWindow::makeConnections()
{
    connect(updateButton, SIGNAL(clicked(bool)), this, SLOT(update_slot()));
    connect(cancelButton, SIGNAL(clicked(bool)), this, SLOT(cancel_slot()));
}

void ChangeWindow::update_slot()
{
    if (old_data == nullptr)
    {
        return;
    }

    QWidget* parent_base = parentWidget();
    if (MainWindow* parent_proper = dynamic_cast<MainWindow*>(parent_base))
    {
        Database* db = parent_proper->getDatabase();
        if (db->connect())
        {
            std::unordered_multimap<const DataSet::DataType, QVariant> new_data_map;
            new_data_map.emplace(DataSet::DataType::TEXT, new_price_edit->text());
            new_data_map.emplace(DataSet::DataType::TEXT, new_make_edit->text());
            new_data_map.emplace(DataSet::DataType::TEXT, new_size_edit->text());
            new_data_map.emplace(DataSet::DataType::TEXT, new_category_edit->text());
            new_data_map.emplace(DataSet::DataType::TEXT, new_name_edit->text());

            DataSet new_data(new_data_map);

            if (db->updateData(db->getTablesCreated().at(0), *old_data, new_data))
            {
                std::vector<QLineEdit*> new_edits {new_name_edit, new_category_edit, new_size_edit, new_make_edit, new_price_edit};

                for (auto& new_edit : new_edits)
                {
                    new_edit->setText("");
                }

                emit dataChanged();
            }
            else
            {
                QMessageBox msg(this);
                msg.setText("Could not update database.");
                msg.setStandardButtons(QMessageBox::Ok);
                msg.exec();
            }
        }
        else
        {
            QMessageBox msg(this);
            msg.setModal(true);
            msg.setStandardButtons(QMessageBox::Ok);
            msg.setText("Could not connect to database!");
            msg.exec();
        }
    }
    else
    {
        QMessageBox msg(this);
        msg.setModal(true);
        msg.setStandardButtons(QMessageBox::Ok);
        msg.setText("dynamic_cast<MainWindow*> failure -> contact with developer!");
        msg.exec();
    }
}

void ChangeWindow::cancel_slot()
{
    close();
}

