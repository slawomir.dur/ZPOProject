TEMPLATE=app
TARGET=ZPOProject

QT=core gui

greaterThan(QT_MAJOR_VERSION, 4):QT+=widgets

QT+=sql

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    producttable.cpp \
    qdatabasewrapper.cpp

HEADERS += \
    mainwindow.h \
    producttable.h \
    qdatabasewrapper.h
