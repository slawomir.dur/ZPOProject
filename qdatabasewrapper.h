#ifndef QDATABASEWRAPPER_H
#define QDATABASEWRAPPER_H

#include <QVariantList>
#include <QSqlDatabase>
#include <string>
#include <exception>
#include <memory>
#include <vector>
#include <unordered_map>

using TableName = std::string;

// Forward declarations
class DataSet;
class DatabaseTable;

class Database
{
// Possible exceptions - can be turned off
public:
    class DatabaseBaseException : public std::exception
    {
    protected:
        std::string msg_;
      public:
        explicit DatabaseBaseException(const char* message) : msg_(message) {}
        explicit DatabaseBaseException(const std::string& message) : msg_(message) {}
        virtual ~DatabaseBaseException() {}
        virtual const char* what() const throw() override { return msg_.c_str(); }
    };
    class DatabaseNonExistentError : public DatabaseBaseException
    {
      public:
        explicit DatabaseNonExistentError(const char* message) : DatabaseBaseException(message) {}
        explicit DatabaseNonExistentError(const std::string& message) : DatabaseBaseException(message) {}
        virtual ~DatabaseNonExistentError() {}
        virtual const char* what() const throw() override { return msg_.c_str(); }
    };
    class DatabaseClosedError : public DatabaseBaseException
    {
      public:
        explicit DatabaseClosedError(const char* message) : DatabaseBaseException(message) {}
        explicit DatabaseClosedError(const std::string& message) : DatabaseBaseException(message) {}
        virtual ~DatabaseClosedError() {}
        virtual const char* what() const throw() override { return msg_.c_str(); }
    };
    class TypesError : public DatabaseBaseException
    {
      public:
        explicit TypesError(const char* message) : DatabaseBaseException(message) {}
        explicit TypesError(const std::string& message) : DatabaseBaseException(message) {}
        virtual ~TypesError() {}
        virtual const char* what() const throw() override { return msg_.c_str(); }
    };
    class LackOfDataError : public DatabaseBaseException
    {
      public:
        explicit LackOfDataError(const char* message) : DatabaseBaseException(message) {}
        explicit LackOfDataError(const std::string& message) : DatabaseBaseException(message) {}
        virtual ~LackOfDataError() {}
        virtual const char* what() const throw() override { return msg_.c_str(); }
    };
    class DataAndTypesMismatch : public DatabaseBaseException
    {
    public:
        explicit DataAndTypesMismatch(const char* message) : DatabaseBaseException(message) {}
        explicit DataAndTypesMismatch(const std::string& message) : DatabaseBaseException(message) {}
        virtual ~DataAndTypesMismatch() {}
        virtual const char* what() const throw() override { return msg_.c_str(); }
    };
// Public enums for db Driver and Exceptions
public:
    enum class DatabaseDriver {SQLITE, MYSQL};
    enum class ExceptionHandling {THROW, NOTHROW};
// Private fields
private:
    QString host;
    QString type;
    QString name;
    std::unique_ptr<QSqlDatabase> db;
    std::vector<DatabaseTable> tables_created;
    ExceptionHandling exceptions = ExceptionHandling::THROW;
// Methods
public:
    Database(const QString &host, const DatabaseDriver& driver, const QString &name);
    Database(const char *host, const DatabaseDriver& driver, const char *name);
    Database(const QString& host, const QString& type, const QString& name);
    Database(const char* host, const char* type, const char* name);
    virtual ~Database() { db->close(); }
    bool connect();
    void disconnect();
    bool createTable(const DatabaseTable& table);
    bool insertData(const DatabaseTable& table, const DataSet& data);
    bool insertData(const char* table, const std::vector<std::string>& colnames, const DataSet& data);
    bool insertData(const QString& table, const std::vector<std::string>& colnames, const DataSet& data);
    bool insertData(const std::string& table, const std::vector<std::string>& colnames, const DataSet& data);
    bool insertData(const char* table, const QVariantList& colnames, const DataSet& data);
    bool removeData(const DatabaseTable& table, const DataSet& data);
    bool removeData(const char* table, const std::vector<std::string>& colnames, const DataSet& data);
    bool removeData(const QString& table, const std::vector<std::string>& colnames, const DataSet& data);
    bool removeData(const std::string& table, const std::vector<std::string>& colnames, const DataSet& data);
    bool removeData(const char* table, const QVariantList& colnames, const DataSet& data);
    bool updateData(const DatabaseTable& table, const DataSet& data_to_update, const DataSet& new_data);
    std::vector<DataSet> getData(const DatabaseTable& table,
                                     const char* what_expr = nullptr, const char* where_expr = nullptr);
    std::vector<DatabaseTable> getTableInfo() const;
    std::unordered_multimap<std::string, std::vector<DataSet>> getAllData();
    bool isOpen();
    void setExceptions(const ExceptionHandling& exc)
        { exceptions = (exc == ExceptionHandling::THROW) ? ExceptionHandling::THROW : ExceptionHandling::NOTHROW; }
    const std::vector<DatabaseTable>& getTablesCreated() { return tables_created; }
private:
    bool isTableDataGood(const DatabaseTable& table, const DataSet& data);

};

class DataSet
{
public:
    enum class DataType { NULLTYPE, INTEGER, REAL, TEXT, BLOB };
private:
    QVariantList data;
    unsigned int dataSize;
public:
    DataSet() = default;
    DataSet(const QVariantList& data);
    DataSet(const std::unordered_multimap<const DataSet::DataType, QVariant>& datamap);
    void setData(const QVariantList& newData);
    QVariantList getData() const { return data; }
    void push_back(const QVariant& element) { data.push_back(element); ++dataSize; }
    const unsigned int& size() const;
    friend class Database;
};

class DatabaseTable
{
private:
    QString tablename;
    QVariantList colnames;
    QVariantList coltypes;
    unsigned int size;
public:
    DatabaseTable() = default;
    DatabaseTable(const QString& tablename, const QVariantList& colnames, const QVariantList& coltypes);
    void setTableName(const char* tablename);
    void setTableName(const QString& tablename);
    void setTableName(const std::string& tablename);
    void setColNames(const QVariantList& colnames);
    void setColTypes(const QVariantList& coltypes);
    const unsigned int& Size() const { return size; }
    const QString& getTableName() const;
    const QVariantList& getColNames() const;
    const QVariantList& getColTypes() const;
    friend class Database;
};

#endif // QDATABASEWRAPPER_H
