#include "qdatabasewrapper.h"

#include <regex>
#include <QFile>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlField>

Database::Database(const char *host, const DatabaseDriver &driver, const char *name)
    : host(host), name(name)
{
    if (driver == DatabaseDriver::SQLITE) // only this supported currently
    {
        type = "QSQLITE";
    }
    else if (driver == DatabaseDriver::MYSQL)
    {
        type = "MYSQL";
    }

    db = std::make_unique<QSqlDatabase>(QSqlDatabase::addDatabase(type));

    db->setHostName(host);
    db->setDatabaseName(name);
}

Database::Database(const QString &host, const DatabaseDriver &driver, const QString &name)
    : host(host), name(name)
{
    if (driver == DatabaseDriver::SQLITE)
    {
        type = "QSQLITE";
    }
    else if (driver == DatabaseDriver::MYSQL)
    {
        type = "MYSQL";
    }

    db = std::make_unique<QSqlDatabase>(QSqlDatabase::addDatabase(type));

    db->setHostName(host);
    db->setDatabaseName(name);
}

Database::Database(const QString &host, const QString &type, const QString &name)
    : host(host), type(type), name(name), db(std::make_unique<QSqlDatabase>(QSqlDatabase::addDatabase(type)))
{
    db->setHostName(host);
    db->setDatabaseName(name);
}

Database::Database(const char* host, const char* type, const char* name)
    : host(host), type(type), name(name), db(std::make_unique<QSqlDatabase>(QSqlDatabase::addDatabase(QString(type))))
{
    db->setHostName(host);
    db->setDatabaseName(name);
}

bool Database::connect()
{
    if (QFile(name.toStdString().c_str()).exists())
    {
        if (!db->isOpen())
        {
            return db->open();
        }
        else return true;
    }
    else
    {
        QFile file;
        file.setFileName(name);
        if(file.open(QIODevice::WriteOnly|QIODevice::Text))
        {
            file.close();
            return db->open();
        }
        else
        {
            if (exceptions == ExceptionHandling::THROW)
                throw Database::DatabaseNonExistentError("Database does not exist. Could not connect.");
            else
                return false;
        }
    }
}

void Database::disconnect()
{
    if (db->isOpen())
    {
        db->close();
        return;
    }
    else return;
}

bool Database::createTable(const DatabaseTable &table)
{
    if (!db->isOpen())
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::DatabaseClosedError("Database closed. Cannot create table.");
        else
            return false;
    }
    else if (table.colnames.size() != table.coltypes.size())
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::TypesError("Number of columns does not match that of types.");
        else
            return false;
    }
    else
    {
        std::string queryString("CREATE TABLE IF NOT EXISTS ");
        queryString += table.tablename.toStdString() + "(";

        for (auto col_it = table.colnames.begin(), types_it = table.coltypes.begin();
             col_it != table.colnames.end() || types_it != table.coltypes.end(); ++col_it, ++types_it)
        {
            queryString += col_it->toString().toStdString() + " " + types_it->toString().toStdString() +",";
        }

        queryString.pop_back();
        queryString += ")";

        QString queryQString(queryString.c_str());

        QSqlQuery query;

        if (query.exec(queryQString))
        {
            tables_created.push_back(table);
            return true;
        }
        else
        {
            return false;
        }
    }
}

bool Database::insertData(const DatabaseTable &table, const DataSet &data)
{
    if (data.dataSize == 0)
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::LackOfDataError("There is no data to be inserted.");
        else
            return false;
    }
    else if (table.size != data.dataSize)
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::TypesError("Data mismatch. Cannot perform insert operation into the table.");
        else
            return false;
    }
    else if (!isTableDataGood(table, data))
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::DataAndTypesMismatch("Data mismatch the types.");
        else
            return false;
    }
    else
    {
        std::string querytext("INSERT INTO ");
        querytext += table.tablename.toStdString() + "(";
        for (auto& i : table.colnames)
        {
            querytext += i.toString().toStdString() + ",";
        }
        querytext.pop_back();
        querytext += ") VALUES(";
        for (auto& i : data.data)
        {
            querytext += i.toString().toStdString() + ",";
        }
        querytext.pop_back();
        querytext += ")";

        QSqlQuery query;
        if (query.exec(querytext.c_str()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

bool Database::insertData(const char* table, const std::vector<std::string>& colnames, const DataSet& data)
{
    if (data.dataSize == 0)
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::LackOfDataError("There is no data to be inserted.");
        else
            return false;
    }

    std::string querytext("INSERT INTO ");
    querytext += std::string(table) + "(";

    for (auto& i : colnames)
    {
        querytext += i + ",";
    }

    querytext.pop_back();
    querytext += ") VALUES(";

    for (auto& i : data.data)
    {
        querytext += i.toString().toStdString() + ",";
    }

    querytext.pop_back();
    querytext += ")";

    QSqlQuery query;
    if (query.exec(querytext.c_str()))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Database::insertData(const QString& table, const std::vector<std::string>& colnames, const DataSet& data)
{
    if (data.dataSize == 0)
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::LackOfDataError("There is no data to be inserted.");
        else
            return false;
    }

    // sql hex

    std::string querytext("INSERT INTO ");
    querytext += table.toStdString() + "(";

    for (auto& i : colnames)
    {
        querytext += i + ",";
    }

    querytext.pop_back();
    querytext += ") VALUES(";

    for (auto& i : data.data)
    {
        querytext += i.toString().toStdString() + ",";
    }

    querytext.pop_back();
    querytext += ")";

    QSqlQuery query;
    return query.exec(querytext.c_str());
}

bool Database::insertData(const std::string& table, const std::vector<std::string>& colnames, const DataSet& data)
{
    if (data.dataSize == 0)
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::LackOfDataError("There is no data to be inserted.");
        else
            return false;
    }

    std::string querytext("INSERT INTO ");
    querytext += table + "(";

    for (auto& i : colnames)
    {
        querytext += i + ",";
    }

    querytext.pop_back();
    querytext += ") VALUES(";

    for (auto& i : data.data)
    {
        querytext += i.toString().toStdString() + ",";
    }

    querytext.pop_back();
    querytext += ")";

    QSqlQuery query;
    if (query.exec(querytext.c_str()))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Database::removeData(const DatabaseTable& table, const DataSet& data)
{
    if (table.size != data.dataSize)
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::TypesError("The number of data mismatch that of column types.");
        else
            return false;
    }
    std::string queryString("DELETE FROM ");
    queryString += table.tablename.toStdString() + " WHERE ";

    for (auto names_it = table.colnames.begin(), data_it = data.data.begin();
         names_it != table.colnames.end() || data_it != data.data.end(); ++names_it, ++data_it)
    {
        queryString += names_it->toString().toStdString() + "=" + data_it->toString().toStdString() + " AND ";
    }

    for (int i = 0; i < 5; ++i)
    {
        queryString.pop_back();
    }


    QSqlQuery query;
    if (query.exec(queryString.c_str()))
    {
        return true;
    }
    else return false;
}

bool Database::removeData(const char* table, const std::vector<std::string>& colnames, const DataSet& data)
{
    if (colnames.size() != data.dataSize)
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::TypesError("The number of data mismatch that of column types.");
        else
            return false;
    }
    std::string queryString("DELETE FROM ");
    queryString += std::string(table) + " WHERE ";

    QVariantList collist;
    for (const auto& i : colnames)
    {
        collist << QString(i.c_str());
    }

    for (QList<QVariant>::const_iterator names_it = collist.begin(), data_it = data.data.begin();
         names_it != collist.end() || data_it != data.data.end(); ++names_it, ++data_it)
    {
        queryString += names_it->toString().toStdString() + "=" + data_it->toString().toStdString() + " AND ";
    }

    for (int i = 0; i < 5; ++i)
    {
        queryString.pop_back();
    }

    QSqlQuery query;
    if (query.exec(queryString.c_str()))
    {
        return true;
    }
    else return false;
}

bool Database::removeData(const QString& table, const std::vector<std::string>& colnames, const DataSet& data)
{
    if (colnames.size() != data.dataSize)
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::TypesError("The number of data mismatch that of column types.");
        else
            return false;
    }
    std::string queryString("DELETE FROM ");
    queryString += table.toStdString() + " WHERE ";

    QVariantList collist;
    for (auto& i : colnames)
    {
        collist << QString(i.c_str());
    }

    for (QList<QVariant>::const_iterator names_it = collist.begin(), data_it = data.data.begin();
         names_it != collist.end() || data_it != data.data.end(); ++names_it, ++data_it)
    {
        queryString += names_it->toString().toStdString() + "=" + data_it->toString().toStdString() + " AND ";
    }

    for (int i = 0; i < 5; ++i)
    {
        queryString.pop_back();
    }

    QSqlQuery query;
    if (query.exec(queryString.c_str()))
    {
        return true;
    }
    else return false;
}

bool Database::removeData(const std::string& table, const std::vector<std::string>& colnames, const DataSet& data)
{
    if (colnames.size() != data.dataSize)
    {
        if (exceptions == ExceptionHandling::THROW)
            throw Database::TypesError("The number of data mismatch that of column types.");
        else
            return false;
    }

    std::string queryString("DELETE FROM ");
    queryString += table + " WHERE ";

    QVariantList collist;
    for (auto& i : colnames)
    {
        collist << QString(i.c_str());
    }

    for (QList<QVariant>::const_iterator names_it = collist.begin(), data_it = data.data.begin();
         names_it != collist.end() || data_it != data.data.end(); ++names_it, ++data_it)
    {
        queryString += names_it->toString().toStdString() + "=" + data_it->toString().toStdString() + " AND ";
    }


    for (int i = 0; i < 5; ++i)
    {
        queryString.pop_back();
    }

    QSqlQuery query;
    if (query.exec(queryString.c_str()))
    {
        return true;
    }
    else return false;
}

bool Database::updateData(const DatabaseTable& table, const DataSet& data_to_update, const DataSet& new_data)
{
    std::string queryString("UPDATE ");
    queryString += table.tablename.toStdString() + " SET ";

    for (QList<QVariant>::const_iterator col_it = table.colnames.begin(), new_data_it = new_data.data.begin();
         col_it != table.colnames.end() || new_data_it != new_data.data.end(); ++col_it, ++new_data_it)
    {
        queryString += col_it->toString().toStdString() + "=" + new_data_it->toString().toStdString() + ",";
    }

    queryString.pop_back();

    queryString += " WHERE ";

    for (QList<QVariant>::const_iterator col_it = table.colnames.begin(), old_data_it = data_to_update.data.begin();
         col_it != table.colnames.end() || old_data_it != data_to_update.data.end(); ++col_it, ++old_data_it)
    {
        queryString += col_it->toString().toStdString() + "=" + old_data_it->toString().toStdString() + " AND ";
    }

    for (int i = 0; i < 4; ++i)
    {
        queryString.pop_back();
    }

    QSqlQuery query;
    if (query.exec(queryString.c_str()))
    {
        return true;
    }
    else return false;
}

// frightfully unsafe
std::vector<DataSet> Database::getData(const DatabaseTable& table,
                                 const char* what_expr, const char* where_expr)
{
    std::string queryString("SELECT ");
    std::vector<std::unique_ptr<DataSet>> result;
    QSqlQuery query;

    if (what_expr != nullptr && where_expr != nullptr)
    {
        queryString += what_expr;
        queryString += " FROM ";
        queryString += table.tablename.toStdString();
        queryString += " WHERE ";
        queryString += where_expr;
    }
    else if (what_expr == nullptr && where_expr == nullptr)
    {
        queryString += "* FROM " + table.tablename.toStdString();
    }
    else if (what_expr == nullptr && where_expr != nullptr)
    {
        queryString += "* FROM " + table.tablename.toStdString() + " WHERE " + where_expr;
    }
    else
    {
        queryString += what_expr;
        queryString += " FROM " + table.tablename.toStdString();
    }

    if (query.exec(queryString.c_str()))
    {
        while (query.next())
        {
            int limit = query.record().count();
            result.push_back(std::make_unique<DataSet>());
            for (int i = 0; i < limit; ++i)
            {
                result.back()->push_back(query.record().value(i));
            }
        }
    }
    else
    {
        if (exceptions == ExceptionHandling::THROW)
        {
            throw DatabaseBaseException("Query execution failed.");
        }
        else
        {
            return std::vector<DataSet>();
        }
    }

    std::vector<DataSet> result_vector;
    for (auto& i : result)
    {
        result_vector.push_back(*i);
    }

    return result_vector;
}

std::vector<DatabaseTable> Database::getTableInfo() const
{
    auto list = db->tables();

    std::vector<QSqlRecord> records;
    std::vector<DatabaseTable> tables(list.size());

    int count{};
    for (auto tab_it = tables.begin(); tab_it != tables.end(); ++tab_it)
    {
        tab_it->setTableName(list.at(count));
        records.push_back(db->record(tab_it->tablename));
        ++count;
    }
    auto tab_it = tables.begin();
    for (auto rec_it = records.begin(); rec_it != records.end() || tab_it != tables.end(); ++rec_it, ++tab_it)
    {
        int end = rec_it->count();
        for (int i = 0; i < end; ++i)
        {
            tab_it->colnames.push_back(rec_it->fieldName(i));
            ++(tab_it->size);

            if (QString(rec_it->field(i).value().typeName()) == QString("QString"))
            {
                tab_it->coltypes.push_back("TEXT");
            }
            else if (QString(rec_it->field(i).value().typeName()) == QString("int"))
            {
                tab_it->coltypes.push_back("INTEGER");
            }
            else
            {
                if (exceptions == ExceptionHandling::THROW)
                {
                    // throw something
                }
                else
                    return std::vector<DatabaseTable>();
            }
        }
    }

    return tables;
}

std::unordered_multimap<std::string, std::vector<DataSet>> Database::getAllData()
{
    auto tables = this->getTableInfo();
    std::unordered_multimap<std::string, std::vector<DataSet>> result;
    for (auto& table: tables)
    {
        auto data = this->getData(table);
        result.emplace(table.tablename.toStdString(), data);
    }
    return result;
}

bool Database::isOpen()
{
    return db->isOpen();
}

bool Database::isTableDataGood(const DatabaseTable &table, const DataSet &data) // to fix
{
    auto data_set = data.data;
    auto coltypes_set = table.coltypes;
    for (auto data_it = data_set.begin(), table_it = coltypes_set.begin();
         data_it != data_set.end() || table_it != coltypes_set.end(); ++data_it, ++table_it)
    {
        const auto& type = table_it->toString();
        if (type == "NULL")
        {
            return false;
        }
        else if (type == "TEXT")
        {
            auto element = data_it->toString();
            if (element.startsWith('\"', Qt::CaseInsensitive) && element.endsWith('\"', Qt::CaseInsensitive))
            {
                return true;
            }
            else
            {
                element.insert(0, '\"');
                element.insert(element.size()-1, '\"');

                *data_it = element;
            }
        }
        else if (type == "INTEGER")
        {
            return false;
        }
        else if (type == "REAL")
        {
            return false;
        }
    }
    return true;
}

DataSet::DataSet(const QVariantList &data)
    : data(data), dataSize(data.size())
{}

DataSet::DataSet(const std::unordered_multimap<const DataSet::DataType, QVariant>& datamap)
{
    for (auto& pair : datamap)
    {
        if (pair.first == DataType::TEXT)
        {
            QString string(pair.second.toString());
            if (string.startsWith('\"', Qt::CaseInsensitive) && string.endsWith('\"', Qt::CaseInsensitive))
            {
                this->data.push_back(pair.second);
            }
            else
            {
                string.insert(0, '\"');
                string.insert(string.size(), '\"');
                this->data.push_back(string);
            }
        }
    }
    dataSize = this->data.size();
}

void DataSet::setData(const QVariantList &newData)
{
    data = newData;
    dataSize = data.size();
}

const unsigned int &DataSet::size() const
{
    return dataSize;
}

DatabaseTable::DatabaseTable(const QString &tablename, const QVariantList &colnames, const QVariantList &coltypes)
    : tablename(tablename), colnames(colnames), coltypes(coltypes), size(colnames.size())
{}

void DatabaseTable::setColNames(const QVariantList &colnames)
{
    this->colnames = colnames;
    this->size = colnames.size();
}

void DatabaseTable::setColTypes(const QVariantList &coltypes)
{
    this->coltypes = coltypes;
}

void DatabaseTable::setTableName(const char *tablename)
{
    this->tablename = tablename;
}

void DatabaseTable::setTableName(const QString &tablename)
{
    this->tablename = tablename;
}

void DatabaseTable::setTableName(const std::string &tablename)
{
    this->tablename = tablename.c_str();
}

const QVariantList& DatabaseTable::getColNames() const
{
    return colnames;
}

const QVariantList& DatabaseTable::getColTypes() const
{
    return coltypes;
}

const QString& DatabaseTable::getTableName() const
{
    return tablename;
}
